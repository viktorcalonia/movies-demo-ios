Movies Demo
-----------

This project is basic master-detail app that will show movies from "https://itunes.apple.com"

This project displays basic requirements for building an app:
- Remote data decoding
- Remote api interaction
- Table View Setup
- UIKit, Cocoa, and Interface Builder knowledge
- Integration Testing
- Unit Testing
- RxSwift as callback

This project also displays the following coding style:
- MVVM
- SOLID
- OOP
- Composition
- Dependency Injection
- Strategy
- Singleton
- Protocol Oriented
- Testability of Code for Unit Testing

Dependencies

- Alamofire, '4.8.2'
- PureLayout, '3.1.6'
- RxSwift, '~> 5'
- RxCocoa, '~> 5'
- NSObject+Rx, '5.1.0'
- SDWebImage, '~> 5.0'
- SVProgressHUD,'~> 2.2.5'
- Quick, '~> 2.1.0'
- Nimble, '~> 8.0.2'
- RxNimble, '~> 4.7.1'
- Mockit, '1.5.0'