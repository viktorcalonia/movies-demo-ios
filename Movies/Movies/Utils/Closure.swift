//
//  Closure.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

public typealias EmptyResult<ReturnType> = () -> ReturnType

// Custom Result + Custom Return
public typealias SingleResultWithReturn<T, ReturnType> = ((T) -> ReturnType)
public typealias DoubleResultWithReturn<T1, T2, ReturnType> = ((T1, T2) -> ReturnType)
public typealias TripleResultWithReturn<T1, T2, T3, ReturnType> = ((T1, T2, T3) -> ReturnType)
// Max limit should be three arguments only

// Custom Result + Void Return
public typealias SingleResult<T> = SingleResultWithReturn<T, Void>
public typealias DoubleResult<T1, T2> = DoubleResultWithReturn<T1, T2, Void>
public typealias TripleResult<T1, T2, T3> = TripleResultWithReturn<T1, T2, T3, Void>
// Max limit should be three arguments only

// Common
public typealias VoidResult = EmptyResult<Void> // () -> Void
public typealias ErrorResult = SingleResult<Error> // (Error) -> Void
public typealias BoolResult = SingleResult<Bool> // (Bool) -> Void
