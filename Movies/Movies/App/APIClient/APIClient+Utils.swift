//
//  APIClient+Utils.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

import Alamofire

enum HTTPRequestHeaderContentType: String {
  case json = "application/json"
  case urlEncoded = "application/x-www-form-urlencoded"
}

enum APIClientResult<Value> {
  case success(Value)
  case failure(Error)
}

extension APIClient {

  func endpointURL(_ resourcePath: String) -> URL {
    return baseUrl.appendingPathComponent(resourcePath)
  }

  func httpRequestHeaders(
    withAuth: Bool = true,
    contentType: HTTPRequestHeaderContentType = .urlEncoded
  ) -> [String: String] {
    var headers = [
      "Content-Type": contentType.rawValue
    ]
    if contentType == .json {
      headers["Accept"] = contentType.rawValue
    }
    return headers
  }
}

extension APIClient {

  public func apiRequest(
    _ resourcePath: String,
    method: HTTPMethod = .get,
    version: String? = nil,
    parameters: Parameters? = nil,
    encoding: ParameterEncoding = URLEncoding.default,
    headers: HTTPHeaders? = nil) -> DataRequest {
    return Alamofire
      .request(
        endpointURL(resourcePath),
        method: method,
        parameters: parameters,
        encoding: encoding,
        headers: headers ?? httpRequestHeaders(withAuth: true)
    )
  }

}

extension DataRequest {

  @discardableResult
  func apiResponse<T>(
    queue: DispatchQueue? = nil,
    completion: @escaping SingleResult<APIClientResult<T>>
  ) -> DataRequest where T: Decodable {
    return self.responseData(queue: queue, completionHandler: { (response: DataResponse<Data>) in

      guard response.result.error == nil else {
        return completion(.failure(response.result.error!))
      }

      guard let responseData = response.value else {
        return completion(.failure(NSError.errorWithLocalizedDescription("No Data")))
      }

      do {
        let resp = try JSONDecoder().decode(T.self, from: responseData)
        completion(.success(resp))
      } catch {
        completion(.failure(error))
      }
    })
  }

  // TODO Throw error
  private func utf8Data(from data: Data) -> Data {
    let encoding = detectEncoding(of: data)
    guard encoding != .utf8 else { return data }
    guard let responseString = String(data: data, encoding: encoding) else {
      preconditionFailure("Could not convert data to string with encoding \(encoding.rawValue)")
    }
    guard let utf8Data = responseString.data(using: .utf8) else {
      preconditionFailure("Could not convert data to UTF-8 format")
    }
    return utf8Data
  }

  private func detectEncoding(of data: Data) -> String.Encoding {
    var convertedString: NSString?
    let encoding = NSString.stringEncoding(
      for: data,
      encodingOptions: nil,
      convertedString: &convertedString,
      usedLossyConversion: nil
    )
    return String.Encoding(rawValue: encoding)
  }

}
