//
//  APIClient.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

import Alamofire

public class APIClient {

  let baseUrl: URL

  public init(baseUrl: URL) {
    self.baseUrl = baseUrl
  }

}
