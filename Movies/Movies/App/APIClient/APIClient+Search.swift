//
//  APIClient+Search.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

import Alamofire

public protocol GetItunesSearch {
  func getSearch(
    params: SearchParams,
    onSuccess: @escaping SingleResult<[Movie]>,
    onError: @escaping ErrorResult)
}


extension APIClient: GetItunesSearch {

  public func getSearch(
    params: SearchParams,
    onSuccess: @escaping SingleResult<[Movie]>,
    onError: @escaping ErrorResult) {
    let params: Parameters = [
      "term": params.term,
      "media": params.media,
      "country": params.country
    ]
    apiRequest("search",
               parameters: params)
      .apiResponse { (result: APIClientResult<SearchResponse>) in
        switch result {
        case .success(let resp):
          onSuccess(resp.results)
        case .failure(let error):
          onError(error)
        }
    }
  }
}
