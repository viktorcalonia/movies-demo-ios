//
//  SearchResponse.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

struct SearchResponse: Decodable {
  let resultCount: Int
  let results: [Movie]
}
