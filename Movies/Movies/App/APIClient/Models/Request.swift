//
//  Request.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

struct Request {
  var path: String
  var method: Method
  var headers: [String: String] = [
    "User-Agent": "iOS",
    "Content-Type": "application/json"
  ]
  var params: Any?
}
