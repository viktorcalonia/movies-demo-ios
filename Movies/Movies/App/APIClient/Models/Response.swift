//
//  Response.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

struct Response<T> {
  var dictBody: [String: Any]?
  var arrayBody: [[String: Any]]?
  var body: T?
  var headers: [AnyHashable: Any]?
  var statusCode: Int?
  var error: Error?
}

