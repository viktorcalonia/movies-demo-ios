//
//  Method.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

enum Method: String {
  case GET
  case POST
  case PUT
  case DELETE
}
