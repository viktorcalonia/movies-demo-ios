//
//  SearchParams.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

public struct SearchParams {
  var term: String
  var country: String
  var media: String
}
