//
//  App.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

class App {

  static let shared = App()

  let apiClient: APIClient

  init() {
    apiClient = APIClient(baseUrl: URL(string: "https://itunes.apple.com")!)
  }
}
