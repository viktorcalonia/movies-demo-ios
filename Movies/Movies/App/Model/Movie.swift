//
//  Movie.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

public struct Movie: Decodable {

  var trackId: Int
  var collectionName: String?
  var artistName: String
  var trackName: String
  var previewUrl: String
  var artworkUrl30: String
  var artworkUrl60: String
  var artworkUrl100: String
  var collectionPrice: Double
  var trackPrice: Double
  var releaseDate: String
  var primaryGenreName: String
  var contentAdvisoryRating: String
  var shortDescription: String?
  var longDescription: String
  var country: String
  var currency: String
  var trackTimeMillis: Int
}
