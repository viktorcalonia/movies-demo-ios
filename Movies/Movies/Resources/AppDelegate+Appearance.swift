//
//  AppDelegate+Root.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

import SVProgressHUD

extension AppDelegate {

  func setupDefaultAppearance() {
    setupDefaultNavBar()
    setupProgressHUDAppearance()
  }

  private func setupDefaultNavBar() {
    let navBar = UINavigationBar.appearance()

    navBar.barTintColor = .white
    navBar.tintColor = .black
    navBar.titleTextAttributes = [
      NSAttributedString.Key.foregroundColor: UIColor.darkGray as Any,
      NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: .medium) as Any
    ]
    navBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
    navBar.shadowImage = UIImage() // No shadow
    navBar.isTranslucent = false
  }

  private func setupProgressHUDAppearance() {
    SVProgressHUD.setDefaultAnimationType(.native)
    SVProgressHUD.setMinimumSize(CGSize(width: 86, height: 86))

    SVProgressHUD.setDefaultStyle(.custom)
    SVProgressHUD.setBackgroundColor(.lightGray)
    SVProgressHUD.setBackgroundLayerColor(.lightGray)
  }


}
