//
//  VideoPlayerController.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit
import AVKit

class RemoteVideoPlayerController: UIViewController {

  var videoVM: RemoteVideoPlayerViewModelProtocol?

  private func setupVideoPlayer() {
    let playerLayer = AVPlayerLayer()
    playerLayer.videoGravity = .resizeAspectFill
    view.layer.addSublayer(playerLayer)
    playerLayer.frame = view.frame

    if let url = videoVM?.remoteVideoUrl {
      let player = AVPlayer(url: url)
      playerLayer.player = player
      player.play()
    }
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    setupVideoPlayer()
  }
}

