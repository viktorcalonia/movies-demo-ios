//
//  RemoteVideoPlayerViewModelProtocol.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

protocol RemoteVideoPlayerViewModelProtocol {
  var remoteVideoUrl: URL { get }
}

extension URL: RemoteVideoPlayerViewModelProtocol {
  var remoteVideoUrl: URL { self }
}
