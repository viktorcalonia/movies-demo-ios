//
//  PaddingLabel.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

class PaddingLabel: UILabel {

  fileprivate var padding: UIEdgeInsets {
    UIEdgeInsets(top: paddingTop, left: paddingLeft, bottom: paddingBottom, right: paddingRight)
  }

  @IBInspectable var paddingTop: CGFloat = 3
  @IBInspectable var paddingBottom: CGFloat = 3
  @IBInspectable var paddingLeft: CGFloat = 13
  @IBInspectable var paddingRight: CGFloat = 13

  override func drawText(in rect: CGRect) {
    let insets = UIEdgeInsets(top: padding.top, left: padding.left, bottom: padding.bottom, right: padding.right)
    super.drawText(in: rect.inset(by: insets))
  }

  override var intrinsicContentSize: CGSize {
    let size = super.intrinsicContentSize
    return CGSize(width: size.width + padding.left + padding.right,
                  height: size.height + padding.top + padding.bottom)
  }
}
