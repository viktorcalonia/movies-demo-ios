//
//  ItemTableViewModelProtocol.swift
//  PerfectCircles
//
//  Created by Viktor Immanuel Calonia on 9/1/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

import RxSwift

protocol ItemTableViewModelProtocol {

  var items: [ItemViewModelProtocol] { get }

  func loadItems() -> Single<[ItemViewModelProtocol]>
}
