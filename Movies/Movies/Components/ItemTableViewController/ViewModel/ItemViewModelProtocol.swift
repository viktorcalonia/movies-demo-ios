//
//  ItemViewModelProtocol.swift
//  PerfectCircles
//
//  Created by Viktor Immanuel Calonia on 9/1/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

protocol ItemViewModelProtocol {

  var mainText: String { get }
  var subText: String { get  }

  var isSelected: Bool { get }
}
