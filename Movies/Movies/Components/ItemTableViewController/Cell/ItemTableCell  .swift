//
//  ItemTableCell  .swift
//  PerfectCircles
//
//  Created by Viktor Immanuel Calonia on 9/1/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

import PureLayout

class ItemTableCell: UITableViewCell {

  var itemVM: ItemViewModelProtocol? {
    didSet {
      setupView()
    }
  }

  @IBOutlet weak var mainLabel: UILabel?
  @IBOutlet weak var subLabel: UILabel?

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    loadContentView()
  }

  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }

  open func loadContentView() {

  }

  open func setupView() {
    if let vm = itemVM {
      mainLabel?.text = vm.mainText
      subLabel?.text = vm.subText
      isSelected = vm.isSelected
    }
  }

  class func defaultReuseIdentifier() -> String {
    return String(describing: self)
  }
}
