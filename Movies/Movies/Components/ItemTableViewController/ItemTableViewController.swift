//
//  ItemTableViewController.swift
//  PerfectCircles
//
//  Created by Viktor Immanuel Calonia on 9/1/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

import RxSwift
import NSObject_Rx

class ItemTableViewController: UITableViewController, ErrorHandler {

  var listViewModel: ItemTableViewModelProtocol?


  override func viewDidLoad() {
    super.viewDidLoad()

    registerCells()
    initialLoadItems()
  }

  func registerCells() {
    tableView.register(ItemTableCell.self, forCellReuseIdentifier: ItemTableCell.defaultReuseIdentifier())
  }

  func initialLoadItems() {
    listViewModel?
      .loadItems()
      .subscribe(onSuccess: { [weak self] (items) in
        self?.tableView.reloadData()
      }, onError: defaultErrorHandler())
      .disposed(by: rx.disposeBag)
  }
}

extension ItemTableViewController {

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return listViewModel?.items.count ?? 0
  }

  override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    if let cell = cell as? ItemTableCell {
      cell.itemVM = listViewModel?.items[indexPath.row]
    }
  }
}
