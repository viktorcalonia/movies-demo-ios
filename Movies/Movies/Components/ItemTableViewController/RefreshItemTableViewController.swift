//
//  RefreshItemTableViewController.swift
//  PerfectCircles
//
//  Created by Viktor Immanuel Calonia on 9/3/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

import RxSwift
import NSObject_Rx

class RefreshItemTableViewController: ItemTableViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    setupTable()
  }

  func setupTable() {
    let refreshControl = UIRefreshControl()
    refreshControl.addTarget(self, action: #selector(refreshItems), for: .valueChanged)
    self.refreshControl = refreshControl
  }

  @objc
  func refreshItems() {
    listViewModel?
      .loadItems()
      .subscribe(onSuccess: { [weak self] (items) in
        self?.tableView.reloadData()
        self?.refreshControl?.endRefreshing()
        }, onError: defaultErrorHandler())
      .disposed(by: rx.disposeBag)
  }

}
