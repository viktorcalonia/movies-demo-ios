//
//  NSError.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

extension NSError {

  static func errorWithLocalizedDescription(_ desc: String,
                                            domain: String = NSCocoaErrorDomain,
                                            code: Int = 0,
                                            userInfo: [String: Any] = [:]) -> NSError {
    var userInfoMutable: [String: Any] = [NSLocalizedDescriptionKey: desc]
    for (key, value) in userInfo {
      userInfoMutable[key] = value
    }

    let ret = NSError(domain: domain, code: code, userInfo: userInfoMutable)
    return ret
  }

  static func memoryHandlingError() -> NSError {
    return NSError.errorWithLocalizedDescription("Memory Error")
  }

}
