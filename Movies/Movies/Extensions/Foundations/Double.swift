//
//  Double.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

extension Double {
  func currencyValue(currencyCode: String = "USD") -> String {
    let currencyFormatter = NumberFormatter()
    currencyFormatter.numberStyle = .currency
    currencyFormatter.minimumFractionDigits = 2
    currencyFormatter.maximumFractionDigits = 2
    currencyFormatter.currencyCode = currencyCode
    var value = "0.00"
    if let string = currencyFormatter.string(from: NSNumber(value: self)) {
      value = string
    }
    return value
  }
}
