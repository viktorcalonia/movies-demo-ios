//
//  DefaultErrorHandlers.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

import SVProgressHUD

protocol ErrorHandler where Self: UIViewController {

  func defaultErrorHandler() -> (Error) -> Void
}

extension ErrorHandler {

  func defaultErrorHandler() -> (Error) -> Void {
    return { error in
      SVProgressHUD.showError(withStatus: error.localizedDescription)
    }
  }

}
