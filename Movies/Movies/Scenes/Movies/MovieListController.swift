//
//  MovieListController.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

enum MovieListControllerSegue: String {
  case presentMoviePage
}

class MovieListController: RefreshItemTableViewController {

  fileprivate var selectedItemContext: MovieViewModelProtocol?

  override func viewDidLoad() {
    super.viewDidLoad()
    setupPropeties()
    initialLoadItems()
  }

  private func setupPropeties() {
    listViewModel = MovieListViewModel()
    tableView.separatorStyle = .none
    tableView.rowHeight = (UIScreen.main.bounds.width / 3) * 1.5
  }

  override func registerCells() {
    tableView.register(MovieCell.self, forCellReuseIdentifier: MovieCell.defaultReuseIdentifier())
  }

}

extension MovieListController {

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    tableView.dequeueReusableCell(withIdentifier: MovieCell.defaultReuseIdentifier(), for: indexPath)
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let vm = listViewModel?.items[indexPath.row] as? MovieViewModelProtocol {
      selectedItemContext = vm
    }
    performSegue(withIdentifier: MovieListControllerSegue.presentMoviePage.rawValue, sender: self)
  }
}

extension MovieListController {

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == MovieListControllerSegue.presentMoviePage.rawValue,
      let vc = segue.destination as? MoviePageController {
      vc.movieVM = selectedItemContext
    }
  }
}
