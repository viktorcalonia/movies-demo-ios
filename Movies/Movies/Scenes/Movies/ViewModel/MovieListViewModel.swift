//
//  MovieListViewModel.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

import RxSwift

class BaseListViewModel: ItemTableViewModelProtocol {

  var items: [ItemViewModelProtocol] = []

  func loadItems() -> Single<[ItemViewModelProtocol]> {
    return Single<[ItemViewModelProtocol]>.create { [weak self] (singleEvent) -> Disposable in
      guard let s = self else { return Disposables.create() }
      let items = [
        DummyMovieViewModel(),
        DummyMovieViewModel(),
        DummyMovieViewModel(),
      ]
      s.items = items
      singleEvent(.success(items))

      return Disposables.create()
    }
  }
}

class MovieListViewModel: MovieListViewModelProtocol {

  var api: GetItunesSearch = App.shared.apiClient

  var items: [ItemViewModelProtocol] = []

  func loadItems() -> Single<[ItemViewModelProtocol]> {
    return Single<[ItemViewModelProtocol]>.create { [weak self] (singleEvent) -> Disposable in
      guard let s = self else { return Disposables.create() }
      let params = SearchParams(term: "star", country: "au", media: "movie")
      s.api.getSearch(params: params, onSuccess: { (movies) in
        let items = movies.map({ MovieViewModel(movie: $0) })
        s.items = items
        singleEvent(.success(items))
      }) { (error) in
        singleEvent(.error(error))
      }

      return Disposables.create()
    }
  }

}
