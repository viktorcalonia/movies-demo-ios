//
//  MovieViewModel.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

class DummyMovieViewModel: MovieViewModelProtocol {

  var mainText: String = "Title"
  var subText: String = "Some Sub Text Some Sub TextSome Sub TextSome Sub TextSome Sub TextSome Sub TextSome Sub TextSome Sub TextSome Sub TextSome Sub TextSome Sub Text "
  var isSelected: Bool = false

  var rating: String = "PG"
  var price: String = "$10.99"

  var posterImageUrl: URL {
    URL(string: "https://is2-ssl.mzstatic.com/image/thumb/Video128/v4/75/6e/46/756e4680-9493-f58c-3d3f-947d36e5c32a/source/100x100bb.jpg")!
  }

  var previewUrl: URL {
    URL(string: "https://video-ssl.itunes.apple.com/itunes-assets/Video128/v4/6b/cd/60/6bcd60b0-73ce-1a9e-1bf8-d7bcc8d32c10/mzvf_2708740245690387686.640x356.h264lc.U.p.m4v")!
  }

  var genre: String = "Sci-Fi & Fantasy"

}

class MovieViewModel: MovieViewModelProtocol {

  let movie: Movie

  public init(movie: Movie) {
    self.movie = movie
  }

}

extension MovieViewModel {
  var mainText: String { movie.trackName }
  var subText: String { movie.longDescription}
  var isSelected: Bool { false }

  var rating: String { movie.contentAdvisoryRating }
  var price: String { movie.trackPrice.currencyValue(currencyCode: movie.currency) }

  var posterImageUrl: URL { URL(string: movie.artworkUrl100)! }

  var previewUrl: URL { URL(string: movie.previewUrl)! }

  var genre: String { movie.primaryGenreName }

}


