//
//  MovieViewModel.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

protocol MovieViewModelProtocol: ItemViewModelProtocol {

  var rating: String { get }
  var price: String { get }
  var posterImageUrl: URL { get }
  var previewUrl: URL { get }
  var genre: String { get }
}

extension MovieViewModelProtocol {

  func getPosterUrl(withWidth width: Int) -> URL {
    let urlString = posterImageUrl.absoluteString
      .replacingOccurrences(of: "100x100", with: "\(width)x\(width)")
    return URL(string: urlString)!
  }

}
