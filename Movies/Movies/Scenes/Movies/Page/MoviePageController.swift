//
//  MoviePageController.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

import SDWebImage

private enum MoviePageControllerSegue: String {
  case embedVideoPlayer
}

class MoviePageController: UIViewController {

  var movieVM: MovieViewModelProtocol!

  @IBOutlet fileprivate weak var posterImageView: UIImageView?
  @IBOutlet fileprivate weak var titleLabel: UILabel?
  @IBOutlet fileprivate weak var descriptionLabel: UILabel?
  @IBOutlet fileprivate weak var ratingLabel: UILabel?
  @IBOutlet fileprivate weak var priceLabel: UILabel?
  @IBOutlet fileprivate weak var genreLabel: UILabel?

  @IBOutlet fileprivate weak var descriptionHeightALC: NSLayoutConstraint?

  @IBOutlet fileprivate weak var playerContainerView: UIView?

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }

  func setupViews() {
    posterImageView?
      .sd_setImage(with: movieVM.getPosterUrl(
        withWidth: Int(UIScreen.main.bounds.width)))
    titleLabel?.text = movieVM.mainText
    descriptionLabel?.text = movieVM.subText
    ratingLabel?.text = movieVM.rating
    priceLabel?.text = movieVM.price

    ratingLabel?.layer.borderColor = UIColor.white.cgColor
    priceLabel?.layer.borderColor = UIColor.white.cgColor

    let descriptionWidth = UIScreen.main.bounds.width - 40
    descriptionHeightALC?.constant =
      descriptionLabel?.text?.height(
        withConstrainedWidth: descriptionWidth,
        font: descriptionLabel?.font
          ?? UIFont.systemFont(ofSize: 17))
      ?? 30
    view.layoutIfNeeded()
  }
}

extension MoviePageController {

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == MoviePageControllerSegue.embedVideoPlayer.rawValue,
      let vc = segue.destination as? RemoteVideoPlayerController {
      vc.videoVM = movieVM.previewUrl
    }
  }
}
