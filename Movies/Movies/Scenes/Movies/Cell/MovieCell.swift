//
//  MovieCell.swift
//  Movies
//
//  Created by Viktor Immanuel Calonia on 9/5/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import UIKit

import SDWebImage
import PureLayout

class MovieCell: ItemTableCell {

  @IBOutlet private weak var priceLabel: UILabel?
  @IBOutlet private weak var ratingLabel: UILabel?
  @IBOutlet private weak var posterImageview: UIImageView?

  @IBOutlet private weak var imageWidthALC: NSLayoutConstraint?

  private let imageWidth = UIScreen.main.bounds.width

  override func loadContentView() {
    if let view = Bundle.main.loadNibNamed("MovieCell", owner: self, options: nil)?.first as? UIView {
      contentView.addSubview(view)
      view.autoPinEdgesToSuperviewEdges()
      setupLayout()
      setupProperties()
    }
  }

  private func setupLayout() {
    imageWidthALC?.constant = imageWidth / 3
  }

  private func setupProperties() {
    selectionStyle = .none
  }

  override func setupView() {
    super.setupView()

    if let vm = itemVM as? MovieViewModelProtocol {
      priceLabel?.text = vm.price
      ratingLabel?.text = vm.rating
      posterImageview?.sd_setImage(with: vm.getPosterUrl(withWidth: Int(imageWidth / 2)))
    }

  }

}
