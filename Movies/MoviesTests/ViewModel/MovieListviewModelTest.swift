//
//  MovieListviewModelTest.swift
//  MoviesTests
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

import Quick
import Nimble
import Mockit

@testable import Movies

class MovieListViewModelTest: QuickSpec {

  override func spec() {

    describe("Movie List View Model") {

      it("should get items if success api") {
        let sut = MovieListViewModel()
        let api = MockSuccessGetItunesSearch(testCase: self)
        sut.api = api

        let onSuccess: SingleResult<[Movie]> = { items in
          expect(items).toNot(beNil())
        }

        let onError: ErrorResult = { error in
          expect(error).to(beNil())
        }

        _ = sut.loadItems().subscribe()

        let params = SearchParams(term: "star", country: "au", media: "movie")
        api
          .verify(verificationMode: Once())
          .getSearch(params: params, onSuccess: onSuccess, onError: onError)

      }

      it("should fail get items if failed api") {
        let sut = MovieListViewModel()
        let api = MockFailGetItunesSearch(testCase: self)
        sut.api = api

        let onSuccess: SingleResult<[Movie]> = { items in
          expect(items).to(beNil())
        }

        let onError: ErrorResult = { error in
          expect(error).toNot(beNil())
        }

        _ = sut.loadItems().subscribe()

        let params = SearchParams(term: "star", country: "au", media: "movie")
        api
          .verify(verificationMode: Once())
          .getSearch(params: params, onSuccess: onSuccess, onError: onError)
      }
    }
  }

}
