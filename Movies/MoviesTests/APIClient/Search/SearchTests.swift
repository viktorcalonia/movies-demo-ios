//
//  SearchTests.swift
//  MoviesTests
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

import Quick
import Nimble

@testable import Movies

class SearchTests: QuickSpec {

  override func spec() {
    let api = APIClientFactory.make()

    context("Search API Client") {

      it("should get movies") {
        waitUntil(timeout: 10) { (done) in
          let params = SearchParams(term: "star", country: "au", media: "movie")
          api.getSearch(params: params, onSuccess: { (movies) in
            expect(movies).toNot(beNil())
            done()
          }) { (error) in
            expect(error).to(beNil())
          }
        }
      }
    }
  }
}

