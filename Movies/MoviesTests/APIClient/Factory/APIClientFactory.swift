//
//  APIClientFactory.swift
//  MoviesTests
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation

@testable import Movies

class APIClientFactory {

  static func make() -> APIClient {
    return APIClient(baseUrl: URL(string: "https://itunes.apple.com")!)
  }
}
