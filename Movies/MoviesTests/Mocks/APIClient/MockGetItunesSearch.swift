//
//  MockGetItunesSearch.swift
//  MoviesTests
//
//  Created by Viktor Immanuel Calonia on 9/6/20.
//  Copyright © 2020 Viktor Immanuel Calonia. All rights reserved.
//

import Foundation
import XCTest

import Mockit

@testable import Movies

class MockGetItunesSearch: GetItunesSearch, Mock {
  typealias InstanceType = MockGetItunesSearch

  let callHandler: CallHandler

  init(testCase: XCTestCase) {
    callHandler = CallHandlerImpl(withTestCase: testCase)
  }

  func instanceType() -> MockGetItunesSearch { self }

  func getSearch(params: SearchParams, onSuccess: @escaping SingleResult<[Movie]>, onError: @escaping ErrorResult) {
    callHandler.accept(
      nil,
      ofFunction: #function,
      atFile: #file,
      inLine: #line,
      withArgs: params, onSuccess, onError)
  }

}

class MockSuccessGetItunesSearch: MockGetItunesSearch {

  override func getSearch(params: SearchParams, onSuccess: @escaping SingleResult<[Movie]>, onError: @escaping ErrorResult) {
    super.getSearch(params: params, onSuccess: onSuccess, onError: onError)
    onSuccess([])
  }
}

class MockFailGetItunesSearch: MockGetItunesSearch {

  override func getSearch(params: SearchParams, onSuccess: @escaping SingleResult<[Movie]>, onError: @escaping ErrorResult) {
    super.getSearch(params: params, onSuccess: onSuccess, onError: onError)
    onError(NSError.errorWithLocalizedDescription("Fail"))
  }
}
